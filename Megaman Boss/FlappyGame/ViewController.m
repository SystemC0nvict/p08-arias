//
//  ViewController.m
//  FlappyGame
//
//  Created by Neo SX on 3/13/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import "ViewController.h"
#import <SpriteKit/SpriteKit.h>
#import "gameScene.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SKView *spriteView = (SKView*) self.view;
    spriteView.showsDrawCount = YES;
    spriteView.showsNodeCount = YES;
    spriteView.showsFPS = YES;
    spriteView.showsPhysics = YES;
    self.view.multipleTouchEnabled = YES;
    // Do any additional setup after loading the view, typically from a nib.
}

/*-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    SKView *spriteView = (SKView*) self.view;
    if(!spriteView.scene){
        spriteView.showsDrawCount = YES;
        spriteView.showsNodeCount = YES;
        spriteView.showsFPS = YES;
        
        SKScene * back = [[MyScene alloc] initWithSize:SKView.bounds.size];
        back.scaleMode = SKSceneScaleModeAspectFill;
        
        [SKView presentScene:back];

    }
}*/

-(void)viewWillAppear:(BOOL)animated
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    gameScene* game = [[gameScene alloc] initWithSize:CGSizeMake(screenSize.width, screenSize.height)];
    SKView *gameview = (SKView *) self.view;
    [gameview presentScene: game];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)InterfaceOrientation{
    if(InterfaceOrientation == UIInterfaceOrientationLandscapeLeft || InterfaceOrientation == UIInterfaceOrientationLandscapeRight){
        return YES;
    }
    return NO;
}


@end

//
//  gameScene.m
//  FlappyGame
//
//  Created by Neo SX on 3/13/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import "gameScene.h"
#import "scrollBack.h"
static const uint32_t bound_cat = 0x20;
static const uint32_t megaman_cat = 0x1;
static const uint32_t block_cat = 0x2;
static const uint32_t lwall_cat = 0x3;
static const uint32_t rwall_cat = 0x7;
static const uint32_t shot_cat = 0x9;
static const uint32_t dam_cat = 0x11;
static const uint32_t boss_cat = 0x13;


bool onground = false;
bool onl = false;
bool onr = false;
bool firing = false;
bool charging = false;
bool zaction = false;
int xhealth = 8;
int zhealth = 30;
int bcount = 0;
int s = 0;

#define BACK_SCROLLING_SPEED .5
@interface gameScene()

@property BOOL contentCreated;
@property (nonatomic) NSMutableArray *floors;
@property (nonatomic) NSMutableArray *col_r;
@property (nonatomic) NSMutableArray *col_l;
@property (nonatomic) NSMutableArray *shots;
@property (nonatomic) NSMutableArray *walktex;
@property (nonatomic) NSMutableArray *zpound;
//@property (nonatomic) NSMutableArray *walktexl;
@property (nonatomic) NSMutableArray *fires;
@property (nonatomic) NSMutableArray *rocks;
@property (nonatomic) NSMutableArray *zshot;
@property (nonatomic) NSMutableArray *zdash;

@end

@implementation gameScene{
    scrollBack * back;
    SKSpriteNode *megaman;
    SKSpriteNode *zero;
    //SKSpriteNode *floor;
    SKSpriteNode *stick;
    SKSpriteNode *ball;
    SKSpriteNode *a_but;
    SKSpriteNode *b_but;
    //SKSpriteNode *floors[10];
    //SKSpriteNode *col_r;
    //CGPoint mvel;
    CGPoint bvels[10];
    SKTexture *walk1;
    SKTexture *walk2;
    SKTexture *walk1left;
    SKTexture *walk2left;
    SKAction *walkr;
    SKTextureAtlas *walkin;
    
    
    bool stickon;
    int p;
    int e;
    
}

-(void)makeatlas{
    _walktex = [NSMutableArray array];
    //_walktexl = [NSMutableArray array];
    _zpound = [NSMutableArray array];
    _fires =[NSMutableArray array];
    _rocks = [NSMutableArray array];
    _zshot = [NSMutableArray array];
     _zdash = [NSMutableArray array];
    for(int pic = 11; pic < 22; pic++){
        NSString *texn = [NSString stringWithFormat:@"megamanxSpritesheet_%d",pic];
        SKTexture *con = [SKTexture textureWithImageNamed:texn];
        [self.walktex addObject:con];
    }
    
    for(int pic = 56; pic < 58; pic++){
        NSString *texn = [NSString stringWithFormat:@"megamanxSpritesheet_%d",pic];
        SKTexture *con = [SKTexture textureWithImageNamed:texn];
        //SKTexture *tp = [walkin textureNamed:@"con"];
        [self.fires addObject:con];
    }
    
    for(int pic = 1; pic < 6; pic++){
        NSString *texn = [NSString stringWithFormat:@"zerox2_0%d",pic];
        SKTexture *con = [SKTexture textureWithImageNamed:texn];
        [self.zpound addObject:con];
    }
    for(int z =59; z < 62; z++ ){
        NSString *texn = [NSString stringWithFormat:@"zerox2_%d",z];
        SKTexture *con = [SKTexture textureWithImageNamed:texn];
        [self.zshot addObject:con];
    }
    for(int z =51; z < 54; z++ ){
        NSString *texn = [NSString stringWithFormat:@"zerox2_%d",z];
        SKTexture *con = [SKTexture textureWithImageNamed:texn];
        [self.zshot addObject:con];
    }
    
    for(int z =43; z < 49; z++ ){
        NSString *texn = [NSString stringWithFormat:@"zerox2_%d",z];
        SKTexture *con = [SKTexture textureWithImageNamed:texn];
        [self.zdash addObject:con];
    }
   

    
    
   }

- (void)didMoveToView:(SKView *)view{
    if(!self.contentCreated)
    {
        //[self makeBack];
        p = 0;
        e = 10;
        walk1 = [SKTexture textureWithImageNamed:@"walk1"];
        walk2 = [SKTexture textureWithImageNamed:@"walk2"];
        walk1left = [SKTexture textureWithImageNamed:@"walk1left"];
        walk2left = [SKTexture textureWithImageNamed:@"walk2left"];

        walkr = [SKAction repeatActionForever:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:NO restore:NO]];
        _floors = [NSMutableArray array];
        [self makeatlas];
        self.physicsWorld.contactDelegate = self;
        self.physicsWorld.gravity = CGVectorMake(0, -4);
        [self createSceneContents];
        self.contentCreated = YES;
        
    }
}


-(void)createSceneContents
{
   // self.backgroundColor = [SKColor colorWithPatternImage: [UIImage imageNamed:@"mx1intro"]];
    [self makeBack];
    self.scaleMode = SKSceneScaleModeAspectFit;
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsBody.categoryBitMask = bound_cat;
    [self addChild:[self megamanobj]];
    [self addChild:[self Zeromake]];
    //for(int i = 0; i < e; i++){
    [self floormake];
    [self make_lcols];
    [self make_rcols];
    
    a_but = [SKSpriteNode spriteNodeWithImageNamed:@"A_Button_01"];
    [self addChild:a_but];
    a_but.position = CGPointMake(CGRectGetMaxX(self.frame)-100, CGRectGetMinY(self.frame) + 50);
    
    b_but = [SKSpriteNode spriteNodeWithImageNamed:@"B_button_01"];
    [self addChild:b_but];
    b_but.position = CGPointMake(a_but.position.x -80, CGRectGetMinY(self.frame) + 50);
    
    stick = [SKSpriteNode spriteNodeWithImageNamed:@"stick_01"];
    [self addChild:stick];
    stick.position = CGPointMake(CGRectGetMinX(self.frame)+100, CGRectGetMinY(self.frame) + 50);
    
    ball = [SKSpriteNode spriteNodeWithImageNamed:@"ball_01"];
    [self addChild:ball];
    ball.position = stick.position;
    
    stick.alpha = .4;
    ball.alpha = .4;
    a_but.alpha = .6;
    b_but.alpha = .6;
    //}
}



-(SKSpriteNode *)megamanobj
{
    //mvel.x = 0;
   // mvel.y = -20;
    //onground = false;
    megaman = [SKSpriteNode spriteNodeWithImageNamed:@"megamanxSpritesheet_08"];
    
    megaman.physicsBody = [SKPhysicsBody bodyWithTexture:megaman.texture size:megaman.size];
   megaman.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:megaman.size];
    //megaman.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGRectMake(120, CGRectGetMaxY(self.frame)-50, 40, 80)];
    [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"spawn"] resize:NO]];
   // [megaman setTexture:[SKTexture textureWithImageNamed:@"spawn"]];
    megaman.position = CGPointMake(120, CGRectGetMaxY(self.frame)-50);
    //megaman.physicsBody.velocity = CGVectorMake(0, -5);
    megaman.physicsBody.restitution = 0;
    megaman.physicsBody.affectedByGravity = YES;
    megaman.physicsBody.dynamic = YES;
    megaman.physicsBody.allowsRotation = false;
    //megaman.physicsBody.usesPreciseCollisionDetection =YES;
    //[self addChild:megaman];
    megaman.physicsBody.categoryBitMask = megaman_cat;
    megaman.physicsBody.collisionBitMask = block_cat;
    return megaman;
}
-(SKSpriteNode *)Zeromake{
    zero = [SKSpriteNode spriteNodeWithImageNamed:@"zerox2_39"];
    zero.physicsBody = [SKPhysicsBody bodyWithTexture:megaman.texture size:megaman.size];
    zero.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:zero.size];
    zero.position = CGPointMake(CGRectGetMaxX(self.frame) -120, 100);
    zero.physicsBody.restitution = 0;
    zero.physicsBody.affectedByGravity = YES;
    zero.physicsBody.dynamic = YES;
    zero.physicsBody.allowsRotation = false;
    zero.physicsBody.categoryBitMask = boss_cat;
    zero.physicsBody.collisionBitMask = block_cat;
    zero.xScale *= -1;
    return zero;
    
}

-(void)shoot{
    SKSpriteNode *shot = [SKSpriteNode spriteNodeWithImageNamed:@"megamanxSpritesheet_97"];
    shot.physicsBody = [SKPhysicsBody bodyWithTexture:shot.texture size:shot.size];
    shot.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:shot.size];
    //shot.position = CGPointMake(CGRectGetMaxX(megaman.frame), CGRectGetMidY(megaman.frame));
    shot.physicsBody.affectedByGravity = NO;
    shot.physicsBody.dynamic = YES;
    shot.physicsBody.allowsRotation = false;
    shot.physicsBody.usesPreciseCollisionDetection = YES;
    shot.physicsBody.categoryBitMask = shot_cat;
    shot.physicsBody.collisionBitMask = 0;
    
    if( onr == true ){
        //[shot.physicsBody applyImpulse:CGVectorMake(3, 0)];
        shot.position = CGPointMake(CGRectGetMinX(megaman.frame), CGRectGetMidY(megaman.frame));
    }
    else if(onl == true){
        shot.position = CGPointMake(CGRectGetMaxX(megaman.frame), CGRectGetMidY(megaman.frame));
    }
   // else if(megaman.xScale > 0 || onl ==true){
    else if( megaman.xScale > 0){
        shot.position = CGPointMake(CGRectGetMaxX(megaman.frame), CGRectGetMidY(megaman.frame));
    }
    
    else if(megaman.xScale < 0){
        shot.position = CGPointMake(CGRectGetMinX(megaman.frame), CGRectGetMidY(megaman.frame));
    }
    [self.shots addObject:shot];
    [self addChild:shot];
    //NSLog(@"onr %d  onl %d", onr, onl);
    if(onl == true && megaman.xScale < 0 ){
        //shot.xScale *= -1;
        [shot.physicsBody applyImpulse:CGVectorMake(3, 0)];
        
    }
    else if(onr == true && megaman.xScale > 0){
        shot.xScale *= -1;
        [shot.physicsBody applyImpulse:CGVectorMake(-3, 0)];
    }
    else if(onl == false && onr == false && megaman.xScale < 0){
        shot.xScale *= -1;
        [shot.physicsBody applyImpulse:CGVectorMake(-3, 0)];
    }
    else if(onl == false && onr == false && megaman.xScale > 0){
        [shot.physicsBody applyImpulse:CGVectorMake(3, 0)];
    }
    
}

-(void)zshoot
{
    [zero runAction:[SKAction animateWithTextures:_zshot timePerFrame:.2 resize: NO restore:YES]];
    SKSpriteNode *zs = [SKSpriteNode spriteNodeWithImageNamed:@"zerox2_29"];
    zs.physicsBody = [SKPhysicsBody bodyWithTexture:zs.texture size:zs.size];
    zs.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:zs.texture.size];
    
    zs.physicsBody.categoryBitMask = dam_cat;
    zs.physicsBody.collisionBitMask = 0;
    zs.physicsBody.contactTestBitMask = bound_cat;
    zs.physicsBody.affectedByGravity = NO;
    //rock.physicsBody.allowsRotation = YES;
    
    if(zero.xScale > 0){
        zs.position = CGPointMake(CGRectGetMaxX(zero.frame), CGRectGetMidY(zero.frame));
    }
    else{
        zs.position = CGPointMake(CGRectGetMinX(zero.frame), CGRectGetMidY(zero.frame));
    }
    zs.physicsBody.dynamic = YES;
    //[self.zshot addObject:zs];
    [self addChild:zs];
    
    if(zero.xScale > 0){

        [zs.physicsBody applyImpulse:CGVectorMake(20, 0)];
    }
    else{
        zs.xScale *= -1;
        [zs.physicsBody applyImpulse:CGVectorMake(-20, 0)];
    }
}
-(void)gpound{
    [zero runAction:[SKAction animateWithTextures:_zpound timePerFrame:.2 resize: NO restore:YES]];
    CGFloat mx = -20;
    CGFloat my = 20;
    for(int i = 0; i < 4; i++){
        SKSpriteNode *rock = [SKSpriteNode spriteNodeWithImageNamed:@"zerox2_06"];
        rock.physicsBody = [SKPhysicsBody bodyWithTexture:rock.texture size:rock.size];
        rock.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:rock.texture.size];
        rock.position = CGPointMake(CGRectGetMaxX(zero.frame), CGRectGetMinY(zero.frame));
        rock.physicsBody.categoryBitMask = dam_cat;
        rock.physicsBody.collisionBitMask = 0;
        rock.physicsBody.contactTestBitMask = bound_cat;
        rock.physicsBody.affectedByGravity = YES;
        rock.physicsBody.allowsRotation = YES;
        rock.physicsBody.dynamic = YES;
        [self.rocks addObject:rock];
        [self addChild:rock];
        CGVector mov = CGVectorMake(mx,my);
        [rock.physicsBody applyImpulse:mov];
        mx += 10;
    }
    dispatch_time_t wa = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0*NSEC_PER_SEC));
    dispatch_after(wa, dispatch_get_main_queue(), ^(void){
        for(int i = 0; i < 4; i++){
            CGVector mov = CGVectorMake(mx,my);
            SKSpriteNode *tmp = _rocks[i];
            [tmp.physicsBody applyImpulse:mov];
            CGFloat tmx = mx +10;
            //mx += 10;
        }
    });
    
}

-(void)zdasher{
    [zero runAction:[SKAction animateWithTextures:_zdash timePerFrame:.2 resize: NO restore:YES]];
    int dir = 1;
    if(zero.xScale < 0){
        dir = -1;
    }
    dispatch_time_t wa = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.4*NSEC_PER_SEC));
    dispatch_after(wa, dispatch_get_main_queue(), ^(void){
        [zero.physicsBody applyImpulse:CGVectorMake(50 * dir, 0)];
    });
}

-(void)floormake
{
   // if(bvels[p-1].x > self.frame.size.width)
    float fx = 370;
    //for(int f = 0; f < 4; f++){
       SKSpriteNode *floor = [SKSpriteNode spriteNodeWithImageNamed:@"longblock"];
        floor.physicsBody = [SKPhysicsBody bodyWithTexture:floor.texture size:floor.size];
        floor.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:floor.texture.size];
        //floor.physicsBody = [SKPhysicsBody bodyWithBodies:@[ft1,ft2,ft3,ft4]];
 
        floor.position = CGPointMake(fx, 40);
    
        floor.physicsBody.restitution = 0;
        floor.physicsBody.categoryBitMask = block_cat;
        floor.physicsBody.contactTestBitMask = megaman_cat;
        floor.physicsBody.collisionBitMask = megaman_cat;
        floor.physicsBody.affectedByGravity = FALSE;
        floor.physicsBody.dynamic = NO;
        [self.floors addObject:floor];
        [self addChild:floor];
    
}

-(void)make_lcols{
    float heights = 200;
    //for(int i =0; i < 5; i++){
        SKSpriteNode *coll = [SKSpriteNode spriteNodeWithImageNamed:@"longcol"];
        coll.physicsBody = [SKPhysicsBody bodyWithTexture:coll.texture size:coll.size];
        coll.position = CGPointMake(30, heights);
        coll.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:coll.frame.size];
        coll.physicsBody.restitution = 0;
        coll.physicsBody.friction = 1;
        coll.physicsBody.categoryBitMask = lwall_cat;
        coll.physicsBody.contactTestBitMask = megaman_cat;
        coll.physicsBody.collisionBitMask = megaman_cat;
        coll.physicsBody.affectedByGravity = false;
        coll.physicsBody.usesPreciseCollisionDetection =YES;
        coll.physicsBody.dynamic = NO;
        [self.col_l addObject:coll];
        [self addChild:coll];
       // heights += 86;
    //}
}

-(void)make_rcols{
    float heights = 200;
    //for(int i =0; i < 5; i++){
        SKSpriteNode *colr = [SKSpriteNode spriteNodeWithImageNamed:@"longcol"];
        colr.physicsBody = [SKPhysicsBody bodyWithTexture:colr.texture size:colr.size];
        colr.position = CGPointMake(CGRectGetMaxX(self.frame)-30, heights);
        colr.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:colr.frame.size];
        colr.physicsBody.restitution = 0;
        colr.physicsBody.friction = 1;
        colr.physicsBody.categoryBitMask = rwall_cat;
        colr.physicsBody.contactTestBitMask = megaman_cat;
        colr.physicsBody.collisionBitMask = megaman_cat;
        colr.physicsBody.affectedByGravity = false;
        colr.physicsBody.usesPreciseCollisionDetection =YES;
        colr.physicsBody.dynamic = NO;
        [self.col_r addObject:colr];
        [self addChild:colr];
        //heights += 86;
    //}
}

-(void)makeBack{
    back = [scrollBack scrollingNodeWithImageNamed:@"mx1intro" inContainerWidth:self.size.width];
    //[back setScrollSpeed:BACK_SCROLLING_SPEED];
   // [back setAnchorPoint:CGPointZero];
    [self addChild:back];
}

-(void)update:(NSTimeInterval)currentTime
{
    //[megaman run]
    if(megaman.position.x > zero.position.x){
        if(zero.xScale < 0)zero.xScale *= -1;
    }
    else if(megaman.position.x < zero.position.x)
    {
        if(zero.xScale > 0)zero.xScale *= -1;
    }
    
    if(xhealth == 0){
        [megaman removeFromParent];
    }
    else if(zhealth == 0){
        [zero removeFromParent];
    }
    int act = arc4random_uniform(5);
    if(act == 1 && zaction == false){
        zaction = true;
        [self zshoot];
        dispatch_time_t wa = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.0*NSEC_PER_SEC));
        dispatch_after(wa, dispatch_get_main_queue(), ^(void){
            zaction = false;
        });
    }
    else if(act == 2 && zaction == false){
        zaction = true;
        [self gpound];
        dispatch_time_t wa = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0*NSEC_PER_SEC));
        dispatch_after(wa, dispatch_get_main_queue(), ^(void){
            zaction = false;
        });
    }
    else if(act == 3 && zaction == false){
        zaction = true;
        [self zdasher];
        dispatch_time_t wa = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0*NSEC_PER_SEC));
        dispatch_after(wa, dispatch_get_main_queue(), ^(void){
            zaction = false;
        });
    }
    
    if(megaman.physicsBody.velocity.dy == 0 && onground == true){
        if(megaman.physicsBody.velocity.dx > 0){
           // [megaman runAction:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.2 resize:NO restore:NO]];
            //NSLog(@"if");
            
            if(![megaman hasActions]){
                //[megaman runAction:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:NO restore:NO] withKey:@"walking"];
                [megaman runAction:[SKAction animateWithTextures:_walktex timePerFrame:.2]];
            }
        }
        else if(megaman.physicsBody.velocity.dx == 0){
            if(firing == false ){
                [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"megamanxSpritesheet_08"] resize:YES]];
            }
            else{
                
                [megaman runAction:[SKAction animateWithTextures:_fires timePerFrame:.2]];
            }
            //megaman.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:megaman.size];
            //NSLog(@"elif");
        }
        else if(megaman.physicsBody.velocity.dx < 0){
            if(![megaman hasActions]){
                if(megaman.xScale > 0){
                    megaman.xScale = megaman.xScale * -1;
                }
                [megaman runAction:[SKAction animateWithTextures:_walktex timePerFrame:.2]];
                //[megaman runAction:[SKAction animateWithTextures:@[walk1left,walk2left] timePerFrame:.3 resize:NO restore:NO] withKey:@"walking left"];
            }
        }
    }
    if(onground == true && megaman.physicsBody.velocity.dy < -1){
        [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"fall"] resize:NO]];
    }
    if(onground == true && megaman.physicsBody.velocity.dy >0){
        [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"megamanxSpritesheet_24"] resize:YES]];
    }
    if(megaman.physicsBody.velocity.dx == 0 && megaman.physicsBody.velocity.dy != 0){
        if(onl == true){
            [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"slideright"] resize:NO]];
        }
        else if(onr ==true){
            [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"slideright"] resize:NO]];
        }
    }
    
    if(ball.position.x < CGRectGetMidX(stick.frame)){
        if(megaman.xScale > 0){
            megaman.xScale = megaman.xScale * -1;
        }
        if(megaman.physicsBody.velocity.dx > 0){
        //[megaman.physicsBody applyImpulse:CGVectorMake(-megaman.physicsBody.velocity.dx, 0)];
            megaman.physicsBody.velocity = CGVectorMake(0, megaman.physicsBody.velocity.dy);
        }
        [megaman.physicsBody applyImpulse:CGVectorMake(-8, 0)];
    }
    else if(ball.position.x > CGRectGetMidX(stick.frame)){
        if(megaman.xScale < 0){
            megaman.xScale = megaman.xScale * -1;
        }
        if(megaman.physicsBody.velocity.dx < 0){
            megaman.physicsBody.velocity = CGVectorMake(0, megaman.physicsBody.velocity.dy);
           // [megaman.physicsBody applyImpulse:CGVectorMake(-megaman.physicsBody.velocity.dx, 0)];
        }
        [megaman.physicsBody applyImpulse:CGVectorMake(8, 0)];
    }
    
    
    
    
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    //mvel.y = 10;
    
   
        //[megaman.physicsBody applyImpulse:CGVectorMake(5,15)];
        //mvel.y = 10;
        //[megaman.physicsBody applyImpulse:CGVectorMake(5, 5)];
        //[SKAction waitForDuration:3];
        //mvel.y  = 0;
    
    if(CGRectContainsPoint(ball.frame, location)){
        stickon = true;
    }
    else{
        stickon = false;
    }
    
    
    if(CGRectContainsPoint(a_but.frame, location)){
        if(megaman.physicsBody.velocity.dy == 0 ){
            [megaman.physicsBody applyImpulse:CGVectorMake(0, 120)];
        }
        else if(megaman.physicsBody.velocity.dx == 0 && onl == true){
            [megaman.physicsBody applyImpulse:CGVectorMake(15, 80)];
        }
        else if(megaman.physicsBody.velocity.dx == 0 && onr == true){
            [megaman.physicsBody applyImpulse:CGVectorMake(-15, 80)];
        }
        
    }
    
    if(CGRectContainsPoint(b_but.frame, location)){
        [self shoot];
        firing = true;
        dispatch_time_t wa = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0*NSEC_PER_SEC));
        dispatch_after(wa, dispatch_get_main_queue(), ^(void){
            firing = false;
        });
        
        //[self gpound];
        //[self zshoot];
        [self zdasher];
        
    }
    
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    if(stickon ==true){
    
        CGVector vec = CGVectorMake(location.x - stick.position.x, location.y - stick.position.y);
        CGFloat angle = atan2(vec.dy, vec.dy);
        CGFloat degree = angle * (180/M_PI);
        CGFloat length = stick.frame.size.height/2;
    
        CGFloat xdist = sin(angle - 1.57079633)*length;
        CGFloat ydist = cos(angle - 1.57079633)*length;
    
        if(CGRectContainsPoint(stick.frame, location)){
            ball.position = location;
        }
        else{
            //ball.position = CGPointMake(stick.position.x - xdist, stick.position.y + ydist);
            SKAction *res = [SKAction moveTo:stick.position duration:.2];
            res.timingMode = SKActionTimingEaseOut;
            [ball runAction:res];
        }
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    if(stickon ==true){
        SKAction *res = [SKAction moveTo:stick.position duration:.2];
        res.timingMode = SKActionTimingEaseOut;
        [ball runAction:res];
    }
    
    if(CGRectContainsPoint(b_but.frame, location)){
        
        
    }
    
        //megaman.
        //mvel.y = -7;
    
}

-(void)didBeginContact:(SKPhysicsContact *)contact{
    SKPhysicsBody *body1 , *body2;
    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        body1 = contact.bodyA;
        body2 = contact.bodyB;
    }
    else{
        body1 = contact.bodyB;
        body2 = contact.bodyA;
    }
    if((body1.categoryBitMask == megaman_cat)&&(body2.categoryBitMask == block_cat)){
        onground = true;
        //[megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"stand"] resize:YES]];
    }
    if((body1.categoryBitMask == megaman_cat)&&(body2.categoryBitMask == lwall_cat)){
        onl = true;
    }
    if((body1.categoryBitMask == megaman_cat)&&(body2.categoryBitMask == rwall_cat)){
        onr = true;
    }
    if(body1.categoryBitMask == shot_cat){
        if(body2.categoryBitMask == shot_cat){
            SKSpriteNode *tmps = (SKSpriteNode *)body2.node;
            [tmps removeFromParent];
        }
        else if(body2.categoryBitMask == boss_cat){
            zhealth -= 1;
        }
        
    }
    if(body2.categoryBitMask == shot_cat){
        SKSpriteNode *tmps = (SKSpriteNode *)body2.node;
        [tmps removeFromParent];
    }
    if(body2.categoryBitMask == dam_cat){
        if(body1.categoryBitMask == megaman_cat){
            xhealth -= 1;
            //SKSpriteNode *tmps = (SKSpriteNode *)body2.node;
            //[tmps removeFromParent];
        }
        else if(body1.categoryBitMask == bound_cat || body1.categoryBitMask == lwall_cat || body1.categoryBitMask == rwall_cat  ){
            SKSpriteNode *tmps = (SKSpriteNode *)body2.node;
            [tmps removeFromParent];
        }
    }
    if(body1.categoryBitMask == dam_cat){
        SKSpriteNode *tmps = (SKSpriteNode *)body1.node;
        [tmps removeFromParent];
    }
    
    //[megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"stand"] resize:YES]];
    
}
-(void)didEndContact:(SKPhysicsContact *)contact{
    SKPhysicsBody *body1 , *body2;
    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        body1 = contact.bodyA;
        body2 = contact.bodyB;
    }
    else{
        body1 = contact.bodyB;
        body2 = contact.bodyA;
    }
    if((body1.categoryBitMask == megaman_cat)&&(body2.categoryBitMask == block_cat)){
        //onground = false;
    }
    if((body1.categoryBitMask == megaman_cat)&&(body2.categoryBitMask == lwall_cat)){
        onl = false;
    }
    if((body1.categoryBitMask == megaman_cat)&&(body2.categoryBitMask == rwall_cat)){
        onr = false;
    }

}

@end

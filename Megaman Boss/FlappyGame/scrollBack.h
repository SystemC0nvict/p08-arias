//
//  scrollBack.h
//  FlappyGame
//
//  Created by Neo SX on 3/15/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface scrollBack : SKSpriteNode
@property (nonatomic) CGFloat scrollSpeed;
+(id) scrollingNodeWithImageNamed:(NSString *) mx1intro inContainerWidth:(float) width;
- (void) update: (NSTimeInterval)currentTime;

@end
